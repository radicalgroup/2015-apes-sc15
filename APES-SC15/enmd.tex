\section{EnsembleMD Toolkit}

There are many science and engineering applications that can benefit from the ability to exploit task-level parallelism. This includes the commonly known category of a “bag-of-tasks” (BoT) where by definition the tasks are uncoupled. However, the degree-of-coupling between the “many tasks” does not necessarily need to be zero (BoT) but can differ, measured by frequency of interaction and volume of exchange between them. Based on the problem statement at hand, patterns with several BoTs with different degrees of coupling may be required. Developing such patterns might require familiarity with new underlying frameworks, a number of which already exist. But a toolkit for using and sharing such developments, through a common easy-to-understand API, is missing and could be useful to vast number of users as it would distinctly separate the role of users and developers. This parition of roles gives the freedom for the developers to make execution level decisions of these tasks such that they do not effect the logic of the application. \\

Every resource has a different architecture and differs in mechanisms and policies surrounding the various actions - job submission, execution, data movement, etc. The specifications of the resource may, in principle, influence the design decisions of the developer as well as the user. This implies, a resource specific implementation which not only forces a different pattern development for each resource but also impedes the possibility of using multiple resources. The solution to this is to use an underlying framework that also provides interoperability and hence can keep the implementation resource agnostic. For this reason, we use RADICAL Pilot as the underlying framework of the EnsembleMD Toolkit. \\

We present the Ensemble MD Toolkit (EnMD Toolkit), a Python framework for developing and executing applications that are comprised of bags of tasks (BoTs) or, in the science community, an ensemble of simulations. EnsembleMD toolkit was designed with the goal to provide an intuitive programming and job execution framework for modelling and execution of large-scale molecular dynamics (MD) applications. The software has been designed top-down, starting from the top with the building blocks (API?) exposed to the user, support for the common patterns in the field of molecular dynamics down to the specifications for the execution using the MD tools. The actual elements which address the complexities in deployment and execution are hidden from the user behind these high-level concepts. The result is an architecture that implements a domain-specific framework for defining and executing large-scale molecular dynamics applications. Communities requiring specific patterns can develop them using RADICAL Pilot and integrate it with the EnMD Toolkit, while users, who simply aim to use available patterns can do so without any involvement in the lower layers. \\

The EnMD toolkit was designed with the following objectives in mind: \\

\begin{enumerate}
\item Concentrate on the science application: The toolkit exposes only simple building blocks, with meaningful nomenclature, which enable the user to define, solely, the design of the application, the executables and the associated data movement. The idea is to keep the translation of an application from a paper/board to an actual program simple, quick and meaningful.

\item Decouple “what to execute” from “how to execute”: A common observable path of construction of scientific applications consists of (1) Definition of the control flow of the application (application logic / algorithm), (2) definition of the executable workload within the defined control flow using the building blocks of a workload management system (“jobs”), and (3) execution of the workload through the services provided by the workload management system. A potential, often overlooked drawback of this straight-forward approach is, that the execution of the executable unit (usually jobs) is solely determined by the application logic and the algorithm, without taking the architecture and the properties of the distributed resources into account on which the application is being executed. This purely application logic-driven execution can often lead to suboptimal application execution with potential room for optimization left untapped. These include lack of concurrent execution, lack of overlapping data movement and computation, etc. It does not also give room to resource specific optimizations such as linking instead of copying input/output data, possible in-memory file storage instead of disk-based storage. A second reason for this partition is to allow different execution strategies for different patterns to be usable with minimal changes to the user interface, which enables the user to execute and compare workloads under different patterns.

\item Hide the heterogeneity: System heterogeneity is one of the biggest practical issues encountered in distributed high-performance computing. Not only do different HPC resource have different interfaces for job submission, file-transfer and accounting, etc. but they also often provide different versions and flavors of common scientific applications. Therfore, there are two levels of heterogeneity that need to be addressed. Firstly, the heterogeneity in the execution methods of the various target resources, i.e. resource-level heterogeneity. The complication of execution in different systems should abstracted out in the lower layers. In Ensemble MD, this is handled by the SAGA layer which is used by RADICAL Pilot as a means of interoperability. Second, heterogeneity in the specific versions (and their dependencies) of the scientific tools, i.e. Kernel level heterogeneity. In Ensemble MD, this requirement is addressed via ”Kernel Plugin” that use a pre-defined set of dictionaries that specify the modules and dependencies to be loaded on the different machines. At the user level, there should be no specific descriptions required when shifting from one resource to another. 

\end{enumerate}


\subsection{Architecture}

Based on the objectives laid out in the previous section, we have designed a modular architecture for Ensemble MD. One of the design goals for EnsembleMD was not to reinvent the wheel and to draw on existing tools, libraries and frameworks wherever possible. Hence, the entire management of distributed HPC resources is handled by RADICAL-Pilot, a user-space Pilot-Job system and SAGA-Python, a low-level resource access library. Both are available via PyPi and allow us to transparently utilize different HPC systems.


\begin{figure}[H]
\centering
	\includegraphics[scale=.27]{./FIGS/enmdtk_arch}
	\caption{EnsembleMD Toolkit Architecture}
	\label{fig:enmd_arch}
\end{figure}

The EnsembleMD architecture consists of four components: the execution patterns, the execution plugins, the execution contexts and the kernels (Figure 1). We describe each of the four components and their implementation as follows:

\begin{enumerate}
\item Execution Patterns: A pattern is a high-level object that represents an application control flow that describes “what to do”. A pattern can be seen as a parameterized template for an execution trajectory that implements a specific algorithm. A pattern provides placeholder methods for the individual steps and stages of an execution trajectory. These placeholders are populated with Kernels that get executed when it’s the step’s turn to be executed. Hence, this is the object that the user interacts with and defines the various MD tools to be executed during the various steps of the pattern. The different execution patterns currently supported by the toolkit are discussed in greater detail in the next section.

\item Execution Plugins: Ensemble MD Toolkit separates the application expression from the execution strategy. The user facing script, i.e. the execution pattern, expresses the ensemble-based execution patterns while the management of its execution is described by ”Execution Plugins”. Decoupling the execution from the expression of the pattern allows the execution plugins to analyze an application’s control- and dataflow, combine the results with existing information about the execution resource and execute according to the derived strategy. Keeping the execution strategy separated from its expression also enables experimenting with different execution strategies without affecting the user application/workload. The execution plugins are created using RADICAL Pilot constructs.

\item Execution Context: The execution context can be see as a container that acquires the resources on the remote machine and provides application level control of these resources. The execution context is the component that provides resource level abstractions and provides a uniform method to access different HPC clusters which may differ in their architecture. It, thus, becomes the component responsible for providing resource-level heterogeneity. It is constructed with the information required to access the desired HPC cluster, i.e., its address, user credentials and core requirements, and URL to a database for bookkeeping.

\item Kernel: A kernel is an object that abstracts a computational task in EnsembleMD. It represents an instantiation of a specific science tool along with its resource specific environment. Kernel plugins hide tool-specific peculiarities across different clusters as well as differences between the interfaces of the various MD tools to the extent possible.
\end{enumerate}


\subsection{EnsembleMD Interface}

Now that we have discussed the various components of the EnMD toolkit, in this subsection we take a glimpse into the EnMD API.

Every EnMD script would require an execution context to be created. It would require details about the target resource - resource name, number of cores, walltime of the reservation, username for access, allocation id, submission queue (optional) and details about a mongo database instance - database url, database name (optional). Following is an example:

\begin{lstlisting}
from radical.ensemblemd import SingleClusterEnvironment

resource = SingleClusterEnvironment(
    resource        = "xsede.stampede", 
    cores           = 16,                   
    walltime        = 30,                
    username        = "abc",            
    allocation      = "TG-xyz123",
    queue           = "normal",      #optional
    database_url    = "mongodb://user:pwd@domain",
    database_name   = "myexps"    #optional
)
\end{lstlisting}


This creates an execution context object targeted at the XSEDE Stampede supercomputer with 16 cores for 30 minutes. Three operations can performed using an execution context:

\begin{itemize}
\item \textbf{allocate():} allocates the resources on the HPC cluster.
\item \textbf{run(pattern):} takes a execution pattern instance and executes it on the HPC cluster.
\item \textbf{deallocate():} terminates the job running on the HPC cluster
\end{itemize}

Once the execution context is created and a resource allocation request is made, we can define an execution pattern that needs to be executed on these resources. The internal details, of how the various steps of the pattern are executed, are held within the execution plugins. For the user, it suffices to inherit the base pattern class and define each of the kernels in all the steps of the pattern. There are, by default, three patterns supported by EnMD.

\subsubsection{Pipeline}
The pipeline execution pattern follows a linear and unidirectional flow of data and control, a common pattern that constitutes many usecases. It supports a chain of steps with each step having multiple parallel executing instances. The pattern is such that the step 'i+1' would, in most cases, processes the output of step 'i'. Figure~\ref{fig:pipeline} is a pictorial representation of the pattern defining 'M' iterations of a bag of size 'N'.
\begin{figure}[H]
\centering
	\includegraphics[width=0.48\textwidth]{FIGS/pipeline}
	\caption{Pipeline Execution Pattern}
	\label{fig:pipeline}
\end{figure}

Ensemble MD provides an intuitive method to utilize the Pipeline pattern. Some of the pattern behavior, specifically the degree of parallelism can be controlled via parameters during object creation. For example, a simple instance of the Pipeline pattern with only two pipeline steps and 16 parallel “instances”, would look like this:
\begin{lstlisting}
from radical.ensemblemd import Pipeline

class MyApp(Pipeline):
	def __init__(self, steps, instances):
        Pipeline.__init__(self, steps, instances)

    def step_1(self, instance):
        ..kernel definition..

    def step_2(self, instance):
    	..kernel definition..

app = MyApp(steps=2,instances=16)
\end{lstlisting}

Each step can define its own Kernel along with any data to be moved, this defines the workload of the respective steps. First, 16 instances of "step\_1" are executed followed by 16 instances of "step\_2". The number of steps and the number of instances in a step are defined during the object creation.

\subsubsection{Replica Exchange}

This is Replica Exchange.

\begin{figure}[H]
\centering
	\includegraphics[width=0.48\textwidth]{FIGS/repex}
	\caption{Replica Exchange Pattern}
	\label{fig:repex}
\end{figure}

\subsubsection{Simulation Analysis Loop}
As evident from the name of the pattern, this pattern finds use in molecular dynamics applications where multiple iterations of simulation tools and analysis tools need to be performed. Figure~\ref{fig:saloop} represents the simulation analysis loop pattern with 'n' simulation instances and 'm' analysis instances in each loop. Although, the patterns bears name due to its first usecase in molecular dynamics, it can in principle be used for any application that follows this design by modifying the specific execution kernels. 

\begin{figure}[H]
\centering
	\includegraphics[width=0.48\textwidth]{FIGS/sa}
	\caption{Simulation Analysis Loop Pattern}
	\label{fig:saloop}
\end{figure}

\begin{lstlisting}
from radical.ensemblemd import SimulationAnalysisLoop

class MyApp(SimulationAnalysisLoop):
	def __init__(self, maxiterations, simulation_instances=1, analysis_instances=1):
        SimulationAnalysisLoop.__init__(self, maxiterations, simulation_instances, analysis_instances)

    def pre_loop(self):
    	..kernel definition..

    def simulation_step(self):
    	..kernel definition..

    def analysis_step(self):
    	..kernel definition..

    def post_loop(self):
    	..kernel definition..

app = MyApp(maxiterations=10, simulation_instances=16, analysis_instances=1)
\end{lstlisting}

Each step can define any kernel to be executed during that step along with associated data movement.Submitting this object for execution would generate 16 simulation instances followed by 1 analysis instance in each iteration. There would be 10 iterations in total.


\textbf{section conclusion ...}